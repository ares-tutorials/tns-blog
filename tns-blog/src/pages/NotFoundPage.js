const NotFoundPage = () => (
    <h1>404: Page Not Found!</h1> // When we've simple component, we can remove curly braces , and simple use round braces
);
export default NotFoundPage;