import { useParams } from "react-router-dom"; // HINT: useParams -> is used to read URL paramters. see URL-Parameter in App.js
import articles from './article-content';
import NotFoundPage from "./NotFoundPage";
import { useState, useEffect } from "react"; // Way to add state to function components in modern react.
import axios from 'axios';
import CommentsList from "../components/CommentsList";
import AddCommentForm from "../components/AddCommentForm";
import useUser from "../hooks/useUser";

const ArticlePage = () => {
    // Goal -> When the article page is first loaded ->
    // 1. Make request to server, server sends back num of comments, upvotes.
    // Step 1: Load data from server
    // Step 2. Create more components ( statefull )

    const [articleInfo, setArticleInfo] = useState({ upvotes: 0, comments: [] });
    const { articleId } = useParams();
    const { user, isLoading } = useUser();

    useEffect(() => {
        const loadArticleInfo = async () => {
            const token = user && await user.getIdToken();
            const headers = token ? { authtoken: token } : {};
            const response = await axios.get(`/api/articles/${articleId}`, {
                headers,
            });
            const newArticleInfo = response.data;
            setArticleInfo(newArticleInfo);
        }
        loadArticleInfo();
    }, [articleId]); // useEffect ->  whenever any of the values that are in this array , changes. useEffect will execute the logic the callback function as the first argument. Try removing the array ( 2nd argument) and see infinite updates in front-end. By passing an Empty Array we're telling,we only want to call the fucntion that we're passing , when its first mounted.

    const article = articles.find(article => article.name === articleId);

    // Function : To handle upvoting of an article.
    const addUpvote = async () => {
        const token = user && await user.getIdToken();
        const headers = token ? { authtoken: token } : {};
        console.log(`-------- \n\n headers ${headers}`)
        const response = await axios.put(`/api/articles/${articleId}/upvote`, null, {
            headers
        });
        const updatedArticle = response.data;
        setArticleInfo(updatedArticle);
    }
    // When the article doesn't exist in articles, you'll see undefined errors in browser console.So making sure it using below.
    if (!article) {
        return <NotFoundPage />
    }

    return (
        // JSX here --
        // NOTE :react-fragment <></> In react we can't 
        // return more than one top-level element from a component.
        <>
            <h1>{article.title}</h1>
            <div className="upvotes-section">
                {user
                    ? <button onClick={addUpvote}>Upvote</button>
                    : <button>Log In to upvote</button>}
                <p>This article has {articleInfo.upvotes} upvote(s)</p>
            </div>
            {article.content.map((paragraph, index) => (
                <p key={index}>{paragraph}</p>
            ))}
            {user
                ? <AddCommentForm
                    articleName={articleId}
                    onArticleUpdated={updatedArticle => setArticleInfo(updatedArticle)} />
                : <button>Log In to upvote</button>}
            <CommentsList comments={articleInfo.comments} />
        </>
    )
}
export default ArticlePage;