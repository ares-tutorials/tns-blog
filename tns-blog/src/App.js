import './App.css';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import NavBar from './NavBar';
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import ArticlePage from './pages/ArticlePage';
import ArticlesListPage from './pages/ArticlesListPage';
import NotFoundPage from './pages/NotFoundPage';
import LoginPage from './pages/LoginPage';
import CreateAccountPage from './pages/CreateAccountPage';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
      <NavBar />
      <div id = "page-body">
        <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/about" element={<AboutPage />} />
        <Route path="/articles" element={<ArticlesListPage/>} />
        <Route path="/articles/:articleId" element={<ArticlePage/>} /> {/* NOTE: URL Parameters */}
        <Route path="/login" element={<LoginPage />} />
        <Route path="/create-account" element={<CreateAccountPage />} />
        <Route path="*" element = {<NotFoundPage/>} /> {/* NOTE: astrisk tells, the react router, display this route for all routes except for all above routes, Place this at last of all routes*/}
        </Routes>
      </div>
    </div>
    </BrowserRouter>
    
  );
}

export default App;