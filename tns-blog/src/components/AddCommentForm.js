import { useState } from "react";
import axios from "axios";
import useUser from "../hooks/useUser";

const AddCommentForm = ({ articleName, onArticleUpdated }) => {
    // Step 1. Add JSX
    // Step 2. Add State
    // Step 3. Add function to allow to submit the comment the user entered.
    const [name, setName] = useState('');
    const [commentText, setCommentText] = useState('');
    const { user } = useUser();

    const addComment = async () => {
        const token = user && await user.getIdToken();
        const headers = token ? { authtoken: token } : {};

        const response = await axios.post(`/api/articles/${articleName}/comments`, {
            postedBy: name,
            text: commentText,
        }, { headers, });

        const updatedArticle = response.data;
        onArticleUpdated(updatedArticle);
        // Reset after submission.
        setName('');
        setCommentText('');
    }

    return (
        <div id="add-comment-form">
            <h3>Add a comment</h3>
            {user && <p>You're posting as {user.email}</p>}
            <label>
                Comment:
                <textarea
                    value={commentText}
                    onChange={e => setCommentText(e.target.value)}
                    rows="4"
                    cols="50" />
            </label>
            <button onClick={addComment}> Add Comment</button>
        </div>
    )
}
export default AddCommentForm;