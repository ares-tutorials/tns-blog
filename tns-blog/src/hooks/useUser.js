//  this useUser custom hook is going to do, is allow our components to get immediate access to the currently logged in user. And if the user isn't currently logged in, then they'll be able to know by that fact that the user just won't exist

import { useState, useEffect } from "react";
import {getAuth, onAuthStateChanged} from 'firebase/auth';

const useUser = () => {
    const [user, setUser] = useState(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(()=>{
        const unsubscribe = onAuthStateChanged(getAuth(), user => {
            setUser(user);
            setIsLoading(false);
        });
        return unsubscribe;
    },[]);

    return { user, isLoading};
}

export default useUser;